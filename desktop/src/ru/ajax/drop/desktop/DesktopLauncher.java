package ru.ajax.drop.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ru.ajax.drop.Drop;

public class DesktopLauncher {
	private static final int width = 800;
	private static final int height = 480;
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "GameScreen";
		config.height = height;
		config.width = width;
		new LwjglApplication(new Drop(), config);
	}
}
