package ru.ajax.drop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import static ru.ajax.drop.GameScreen.catched;
import static ru.ajax.drop.GameScreen.game_over;
import static ru.ajax.drop.GameScreen.level;


class GameOverScreen implements Screen{

    private final Drop game;
    private OrthographicCamera camera;
    private int catchered;
    private Music miscer;
    private static Texture textureGameOverScreen;
    private static TextureRegion backGroundTextureGameOverScreen;
    private static Texture newGame;

    GameOverScreen(Drop gam) {
        this.game = gam;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        catchered = (int)catched;
        miscer = Gdx.audio.newMusic(Gdx.files.internal("gOmusic.mp3"));
        textureGameOverScreen = new Texture("GameOverScreenBackGround.jpg");
        backGroundTextureGameOverScreen = new TextureRegion(textureGameOverScreen, 800, 480);
        newGame = new Texture("newgame.png");
    }

    @Override
    public void show() {

    }

    private void miscGmPlay(){
        miscer.play();
    }

    @Override
    public void render(float delta) {

        camera.update();

        game.batch.setProjectionMatrix(camera.combined);
        miscGmPlay();
        game.batch.begin();
        game.batch.draw(backGroundTextureGameOverScreen, 0,0);
        game.batch.draw(game_over, 243, 135);
        game.batch.draw(newGame, 270, 360);
        game.font.draw(game.batch, "Tap to 'New Game' to continue!", 300, 60);
        game.font.draw(game.batch, "Your score is:  " + catchered, 355, 30);
        game.font.draw(game.batch, "Level:" + level, 385, 100);
        game.batch.end();
        if(Gdx.input.isTouched()){
            if(((Gdx.input.getX() > 250 && Gdx.input.getX() < 530) && ((Gdx.input.getY() < 110 && Gdx.input.getY() > 20))
                    ||((Gdx.input.getX() > 500 && Gdx.input.getX() < 1060) && ((Gdx.input.getY() < 220 && Gdx.input.getY() > 40))))||
                    ((Gdx.input.getX() > 600 && Gdx.input.getX() < 1320) && ((Gdx.input.getY() < 274 && Gdx.input.getY() > 50))))
            {
                game.setScreen(new GameScreen(game));
                dispose();
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        catchered = 0;
        catched = 0;
        miscer.stop();
        textureGameOverScreen.dispose();
    }
}
