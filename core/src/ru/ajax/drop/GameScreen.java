package ru.ajax.drop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

class GameScreen implements Screen {
	/**
	 * init 1
	 * Here only textures, sounds, music etc.
	 * All params should be in initialization 2 block
	 */
	//Start init 1
    private final Drop game;
    private Texture drop;
    private Texture bucket;
    static Texture game_over;
    private Sound waterdrop;
    private Music rainMusic;
	//End init 1
    /**
     * init 2
     * Here only params and vars
     * Classes and links should be placed here to
     */
    //Start init 2
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Rectangle bucketRectangle;
    private Vector3 touchPos;
    public static Array<Rectangle> raindrops;
    private int count;
    public static long lastDropTime;
    static long catched;
    private int drop_v = 20;
    private int counter;
    public static int level;
    private float elapsed;
    private TextureRegion backGroundTexture;
    private Texture textureForBackGround;
    private RainSpawner rsClass = new RainSpawner();

    //End init 2

	GameScreen(final Drop gam) {
        this.game = gam;
        camera = new OrthographicCamera();
        /**
         * @param PlayMode, internal, /.read
         *@return gif image
         */
        batch = new SpriteBatch();
        camera.setToOrtho(false, 800, 480);
        touchPos = new Vector3();
		drop = new Texture("droplet.png");
		bucket = new Texture("bucket.png");
        game_over = new Texture("game_over.png");
        textureForBackGround = new Texture("background.jpg");
        backGroundTexture = new TextureRegion(textureForBackGround, 0, 0, 800, 480);
        level = 1;
        bucketRectangle = new Rectangle();
        bucketRectangle.x = 800 / 2 - 64 / 2;
        bucketRectangle.y = 20;
        bucketRectangle.width = 64;
        bucketRectangle.height = 64;
        raindrops = new Array<Rectangle>();
        //spawn 1st drop
        rsClass.start();
		//Music and sounds
		waterdrop = Gdx.audio.newSound(Gdx.files.internal("waterdrop.wav"));
		rainMusic = Gdx.audio.newMusic(Gdx.files.internal("chaosmusic.mp3"));
        rainMusic.setLooping(true);
        rainMusic.play();
        // End of sound and music
	}

    //Just stopping music
    private void ifKilled(){
        rainMusic.stop();
        waterdrop.dispose();
    }
    @Override
	public void render (float delta) {
        elapsed += Gdx.graphics.getDeltaTime();
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(backGroundTexture, 0, 0);
        game.font.draw(game.batch, "Drops catched: " + catched, 0, 480);
        game.font.draw(game.batch, "Level: " + level, 0, 460);
        game.batch.draw(bucket, bucketRectangle.x, bucketRectangle.y);

        for (Rectangle raindrop : raindrops) {
            game.batch.draw(drop, raindrop.x, raindrop.y);
        }
        game.batch.end();
        if(Gdx.input.isTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            bucketRectangle.x = (int) touchPos.x - (64 / 2);

        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            bucketRectangle.x -= 200 * Gdx.graphics.getDeltaTime();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            bucketRectangle.x += 200 * Gdx.graphics.getDeltaTime();
        }
        if(TimeUtils.nanoTime() - lastDropTime > 1500000000){
            rsClass.rainspawnerAsync(level);

        }
        Iterator<Rectangle> iter = raindrops.iterator();
        while (iter.hasNext()){
            Rectangle raindrop = iter.next();
            raindrop.y -= (200 + drop_v) * Gdx.graphics.getDeltaTime();
            if (raindrop.y + 64 < 0){
                iter.remove();
                game.setScreen(new GameOverScreen(game));
                ifKilled();
            }
            if (raindrop.overlaps(bucketRectangle)){
                catched++;
                waterdrop.play();
                iter.remove();
                count++;
                counter++;
                if(counter >= 5){
                    level++;
                    counter = 0;
                    if(level == 5){
                        drop_v -= 600;
                    }
                    if(level == 9){
                        drop_v  = 0;
                    }
                    if(level == 12){
                        drop_v = 0;
                    }
                }
                if(level < 5) {
                    drop_v += 30;
                }
                else if(level >= 5 && level < 9){
                    drop_v += 18;
                }else if(level >= 9 && level < 12){
                    drop_v += 13;
                }else if(level > 12 && level <= 14){
                    drop_v += 10;
                }
                if(level == 15){
                    rainMusic.stop();
                    rainMusic.dispose();
                    game.setScreen(new YouWin(game));
                }
                String countStr = String.valueOf(count);
                Gdx.graphics.setTitle("Your score is: " + countStr);
            }
        }
    }


    @Override
    public void show() {
        rainMusic.play();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
	public void dispose () {
		batch.dispose();
        drop.dispose();
        bucket.dispose();
        waterdrop.dispose();
        rainMusic.dispose();
	}

}

