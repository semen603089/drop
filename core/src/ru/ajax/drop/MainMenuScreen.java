package ru.ajax.drop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

class MainMenuScreen implements Screen{

    private final Drop game;
    private OrthographicCamera camera;
    private Music miscBlake;
    private Texture buttonTexture;
    private Texture mainTexture;
    private static Texture textureGameOverScreen;
    private static TextureRegion backGroundTextureGameOverScreen;

    MainMenuScreen(final Drop gam) {
        game = gam;
        camera = new OrthographicCamera();
        buttonTexture = new Texture("button.png");
        mainTexture = new Texture("mainimage.png");
        camera.setToOrtho(false, 800, 480);
        miscBlake = Gdx.audio.newMusic(Gdx.files.internal("mainmusic.mp3"));
        textureGameOverScreen = new Texture("GameOverScreenBackGround.jpg");
        backGroundTextureGameOverScreen = new TextureRegion(textureGameOverScreen, 800, 480);
    }

    private void playMainMisc(){
        miscBlake.play();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        camera.update();
        playMainMisc();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(backGroundTextureGameOverScreen, 0,0);
        game.batch.draw(buttonTexture, 270, 10);
        game.batch.draw(mainTexture, 210, 300);
        game.font.draw(game.batch, "Created by Semyon Grigoriev, 2017", 550, 20);
        game.batch.end();
        if(Gdx.input.isTouched()){
            if(((Gdx.input.getX() > 250 && Gdx.input.getX() < 550) && ((Gdx.input.getY() < 465 && Gdx.input.getY() > 210))
                    ||((Gdx.input.getX() > 500 && Gdx.input.getX() < 1100) && ((Gdx.input.getY() < 930 && Gdx.input.getY() > 420))))||
                    ((Gdx.input.getX() > 600 && Gdx.input.getX() < 1320) && ((Gdx.input.getY() < 1046 && Gdx.input.getY() > 473))))
            {
                game.setScreen(new GameScreen(game));
                dispose();
            }
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        miscBlake.pause();
    }

    @Override
    public void resume() {
        miscBlake.play();
    }

    @Override
    public void hide() {
        miscBlake.pause();
    }

    @Override
    public void dispose() {
        miscBlake.dispose();
        buttonTexture.dispose();
    }
}
