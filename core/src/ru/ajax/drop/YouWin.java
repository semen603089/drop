package ru.ajax.drop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import static ru.ajax.drop.GameScreen.catched;

class YouWin implements Screen{

    private final Drop game;
    private OrthographicCamera camera;
    private Music youwinmisc;
    private static Texture youwin;
    private static TextureRegion backGroundTextureGameOverScreen;
    private static Texture button;
    private static Texture textureGameOverScreen;

    YouWin(Drop gam) {
        this.game = gam;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        youwinmisc = Gdx.audio.newMusic(Gdx.files.internal("YouWinmisc.mp3"));
        button = new Texture("newgame.png");
        youwin = new Texture("YouWin.png");
        textureGameOverScreen = new Texture("GameOverScreenBackGround.jpg");
        backGroundTextureGameOverScreen = new TextureRegion(textureGameOverScreen, 0, 0, 800, 480);
    }


    @Override
    public void show() {

    }

    private void playWinnerMusic(){
        youwinmisc.play();
    }

    @Override
    public void render(float delta) {

        camera.update();

        game.batch.setProjectionMatrix(camera.combined);
        playWinnerMusic();
        game.batch.begin();
        game.batch.draw(backGroundTextureGameOverScreen, 0,0);
        game.batch.draw(button, 270,360);
        game.batch.draw(youwin, 160, 120);
        game.font.draw(game.batch, "Created by Semyon Grigoriev, 2017", 550, 20);
        game.font.draw(game.batch, "Tap to 'New Game' if you wanna play one more", 270, 100);
        game.batch.end();
        catched = 0;

        if(Gdx.input.isTouched()){
            if(((Gdx.input.getX() > 250 && Gdx.input.getX() < 530) && ((Gdx.input.getY() < 110 && Gdx.input.getY() > 20))
                    ||((Gdx.input.getX() > 500 && Gdx.input.getX() < 1060) && ((Gdx.input.getY() < 220 && Gdx.input.getY() > 40))))||
                    ((Gdx.input.getX() > 600 && Gdx.input.getX() < 1320) && ((Gdx.input.getY() < 274 && Gdx.input.getY() > 50))))
            {
                game.setScreen(new MainMenuScreen(game));
                dispose();
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        youwin.dispose();
        youwinmisc.stop();
        youwinmisc.dispose();
    }
}
