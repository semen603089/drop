package ru.ajax.drop;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;


import static ru.ajax.drop.GameScreen.raindrops;
import static ru.ajax.drop.GameScreen.lastDropTime;

/**
This class for spawn objects
 RainSpawners
 Created by Admin(SG)
 */

public class RainSpawner extends Thread{

    private int level = 1;

    public static void rainspawnAsync(){
        Rectangle raindrop = new Rectangle();
        raindrop.x = MathUtils.random(0, 800-64);
        raindrop.y = 480;
        raindrop.width = 64;
        raindrop.height = 64;
        raindrops.add(raindrop);
        lastDropTime = TimeUtils.nanoTime();
    }

    public static void rainspawnerAsync(int lvl){


        switch(lvl){
            case 1:
                rainspawnAsync();
                break;
            case 2:
                rainspawnAsync();
                break;
            case 3:
                rainspawnAsync();
                break;
            case 4:
                rainspawnAsync();
                break;
            case 5:
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 6:
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 7:
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 8:
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 9:
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 10:
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 11:
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 12:
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 13:
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                break;
            case 14:
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                rainspawnAsync();
                break;
        }
    }




    @Override
    public void run() {
        rainspawnerAsync(level);
    }
}
